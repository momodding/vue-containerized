FROM node:16.17-alpine

ARG user
ARG uid

RUN apk update && \
	apk add --update git

RUN npm install -g @vue/cli

WORKDIR /app
COPY ./package.json ./

RUN npm install

COPY . .

EXPOSE 8080

CMD ["yarn", "serve"]